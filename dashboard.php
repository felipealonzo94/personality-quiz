<?php
    session_start();
    ob_start();
    if(!isset($_SESSION["username"])){     
      /* Redirect to a different page in the current directory that was requested */
        ob_start();
        $host  = $_SERVER['HTTP_HOST'];
        $uri   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
        $extra = 'login.php';
        header("Location: https://$host$uri/$extra", true, 307);
        ob_end_flush();
    }
    ob_end_flush();

?>
<!DOCTYPE HTML>
<!--
	Dimension by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>

<head>
    <title>CPro- Panel de Administracion</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
    <link rel="stylesheet" href="assets/css/main.css" />
    <noscript>
        <link rel="stylesheet" href="assets/css/noscript.css" />
    </noscript>

    <link rel="icon" type="image/png" sizes="16x16" href="assets/img/cpro-negro_Mesa-de-trabajo-1.webp">
    <link rel="icon" type="image/png" sizes="32x32" href="/assets/img/cpro-negro-02.png">
    <link rel="icon" type="image/png" sizes="181x180" href="/assets/img/cpro-negro-03.png">
    <link rel="icon" type="image/png" sizes="193x192" href="/assets/img/cpro-negro-04.png">

</head>

<body class="is-preload">

    <!-- Wrapper -->
    <div id="wrapper">

        <!-- Header -->
        <header id="header">
            <div class="logo">
                <img class="center" src="assets/img/Cpro-LOGO2-1-01.png" href="" alt="Cpro.mx"
                    style="display: center; width:60%;">
            </div>
            <div class="content">
                <div class="inner">
                    <h1>Panel de Control</h1>
                    <p>Bienvenido, <b> <?php echo htmlspecialchars($_SESSION["username"]); ?>
                        </b> selecciona la opción que deseas realizar.</p>
                </div>
            </div>
            <nav>
                <ul>
                    <li><a href="#Lista">Usuarios</a></li>
                    <li><a href="#Excel_download">Excel</a></li>
                    <li><a href="#clean_db">Limpiar BD</a></li>
                    <li><a href="#users">Perfil</a></li>
                    <li><a href="#logout">Cerrar Sesion</a></li>
                </ul>
            </nav>
        </header>

        <!-- Main -->
        <div id="main">
            <!-- Lista -->
            <article id="Lista">
                <h3 class="major">Usuarios Quiz</h3>
                <p> Lista de usuarios que han presentado el <a href="personalityquiz.php">quiz de personalidad.</a></p>

                <div class="table-wrapper">
                    <table>
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Usuario</th>
                                <th>Correo Electronico</th>
                                <th>Registro</th>
                                <th>¿Esta inscrito?</th>
                            </tr>
                        </thead>

                        <?php
                    require_once "config.php";
                    
                    // Check connection
                    if ($mysqli->connect_error) {
                        die("Connection failed: " . $conn->connect_error);
                    }
    
                    $sql = "SELECT s.*, @rownum := @rownum + 1 AS num FROM alumnos s, (SELECT @rownum := 0) r";
                    $result = $mysqli->query($sql);
    
                    if ($result->num_rows > 0) {
                    // output data of each row
                    while($row = $result->fetch_assoc()) {
                    echo "<tbody> <tr><td>" . $row["num"]. "</td><td>" . $row["name"] . "</td><td>" . $row["email"] . "</td><td>" . $row["timestamp"] . "</td><td>"
                    . $row["is_join"]. "</td></tr></tbody>";
                    }
                    echo "</table>";
                    } else { echo "0 results"; }
                    $mysqli->close();
                    ?>
                </div>

            </article>

            <!-- Excel_download -->
            <article id="Excel_download">
                <h2 class="major">Descargar Registros</h2>
                <p>Descargar registros en excel de todos los alumnos que han presentado el quiz de
                    personalidad.</strong></p>
                <ul class="actions">
                    <li><a href="excel_download.php" class="button primary">Descargar</a></li>
                    <li><a href="#" class="button">Cancelar</a></li>
                </ul>
            </article>

            <!-- Clean DB -->
            <article id="clean_db">
                <h2 class="major">Eliminar Registros</h2>
                <p>¿Esta seguro que desea eliminar el registro completo de alumnos que han presentado el quiz de
                    personalidades?<strong> Esta
                        opcion no se puede deshacer.</strong></p>
                <ul class="actions">
                    <li><a href="delete_db.php" class="button primary">Confirmar</a></li>
                    <li><a href="#" class="button">Cancelar</a></li>
                </ul>
            </article>

            <!-- Cerrar Sesion -->
            <article id="logout">
                <h2 class="major">Cerrar Sesion</h2>
                <form method="post" action="#">
                    <p>¿Desea cerrar sesion?</p>
                    <ul class="actions">
                        <li><a href="logout.php" class="button primary">Confirmar</a></li>
                        <li><a href="#" class="button">Cancelar</a></li>
                    </ul>
                </form>
            </article>

            <!-- Usuarios -->
            <article id="users">
                <h2 class="major">Manejo de Usuarios</h2>
                <a href="reset-password.php" class="button">Cambiar Contraseña</a>
                <a href="register.php" class="button">Crear usuario</a>
                <a href="#" class="button">Cancelar</a>
            </article>

            <!-- Elements -->
            <article id="elements">
                <h2 class="major">Elements</h2>

                <!-- Listas -->
                <section>
                    <h3 class="major">Lists</h3>

                    <h4>Unordered</h4>
                    <ul>
                        <li>Dolor pulvinar etiam.</li>
                        <li>Sagittis adipiscing.</li>
                        <li>Felis enim feugiat.</li>
                    </ul>

                    <h4>Alternate</h4>
                    <ul class="alt">
                        <li>Dolor pulvinar etiam.</li>
                        <li>Sagittis adipiscing.</li>
                        <li>Felis enim feugiat.</li>
                    </ul>

                    <h4>Ordered</h4>
                    <ol>
                        <li>Dolor pulvinar etiam.</li>
                        <li>Etiam vel felis viverra.</li>
                        <li>Felis enim feugiat.</li>
                        <li>Dolor pulvinar etiam.</li>
                        <li>Etiam vel felis lorem.</li>
                        <li>Felis enim et feugiat.</li>
                    </ol>
                    <h4>Icons</h4>
                    <ul class="icons">
                        <li><a href="#" class="icon brands fa-twitter"><span class="label">Twitter</span></a></li>
                        <li><a href="#" class="icon brands fa-facebook-f"><span class="label">Facebook</span></a></li>
                        <li><a href="#" class="icon brands fa-instagram"><span class="label">Instagram</span></a></li>
                        <li><a href="#" class="icon brands fa-github"><span class="label">Github</span></a></li>
                    </ul>

                    <h4>Actions</h4>
                    <ul class="actions">
                        <li><a href="#" class="button primary">Default</a></li>
                        <li><a href="#" class="button">Default</a></li>
                    </ul>
                    <ul class="actions stacked">
                        <li><a href="#" class="button primary">Default</a></li>
                        <li><a href="#" class="button">Default</a></li>
                    </ul>
                </section>
                <!-- Tablas -->
                <section>
                    <h3 class="major">Table</h3>
                    <h4>Default</h4>
                    <div class="table-wrapper">
                        <table>
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Description</th>
                                    <th>Price</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Item One</td>
                                    <td>Ante turpis integer aliquet porttitor.</td>
                                    <td>29.99</td>
                                </tr>
                                <tr>
                                    <td>Item Two</td>
                                    <td>Vis ac commodo adipiscing arcu aliquet.</td>
                                    <td>19.99</td>
                                </tr>
                                <tr>
                                    <td>Item Three</td>
                                    <td> Morbi faucibus arcu accumsan lorem.</td>
                                    <td>29.99</td>
                                </tr>
                                <tr>
                                    <td>Item Four</td>
                                    <td>Vitae integer tempus condimentum.</td>
                                    <td>19.99</td>
                                </tr>
                                <tr>
                                    <td>Item Five</td>
                                    <td>Ante turpis integer aliquet porttitor.</td>
                                    <td>29.99</td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="2"></td>
                                    <td>100.00</td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>

                    <h4>Alternate</h4>
                    <div class="table-wrapper">
                        <table class="alt">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Description</th>
                                    <th>Price</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Item One</td>
                                    <td>Ante turpis integer aliquet porttitor.</td>
                                    <td>29.99</td>
                                </tr>
                                <tr>
                                    <td>Item Two</td>
                                    <td>Vis ac commodo adipiscing arcu aliquet.</td>
                                    <td>19.99</td>
                                </tr>
                                <tr>
                                    <td>Item Three</td>
                                    <td> Morbi faucibus arcu accumsan lorem.</td>
                                    <td>29.99</td>
                                </tr>
                                <tr>
                                    <td>Item Four</td>
                                    <td>Vitae integer tempus condimentum.</td>
                                    <td>19.99</td>
                                </tr>
                                <tr>
                                    <td>Item Five</td>
                                    <td>Ante turpis integer aliquet porttitor.</td>
                                    <td>29.99</td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="2"></td>
                                    <td>100.00</td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </section>

                <section>
                    <h3 class="major">Buttons</h3>
                    <ul class="actions">
                        <li><a href="#" class="button primary">Primary</a></li>
                        <li><a href="#" class="button">Default</a></li>
                    </ul>
                    <ul class="actions">
                        <li><a href="#" class="button">Default</a></li>
                        <li><a href="#" class="button small">Small</a></li>
                    </ul>
                    <ul class="actions">
                        <li><a href="#" class="button primary icon solid fa-download">Icon</a></li>
                        <li><a href="#" class="button icon solid fa-download">Icon</a></li>
                    </ul>
                    <ul class="actions">
                        <li><span class="button primary disabled">Disabled</span></li>
                        <li><span class="button disabled">Disabled</span></li>
                    </ul>
                </section>

                <section>
                    <h3 class="major">Form</h3>
                    <form method="post" action="#">
                        <div class="fields">
                            <div class="field half">
                                <label for="demo-name">Name</label>
                                <input type="text" name="demo-name" id="demo-name" value="" placeholder="Jane Doe" />
                            </div>
                            <div class="field half">
                                <label for="demo-email">Email</label>
                                <input type="email" name="demo-email" id="demo-email" value=""
                                    placeholder="jane@untitled.tld" />
                            </div>
                            <div class="field">
                                <label for="demo-category">Category</label>
                                <select name="demo-category" id="demo-category">
                                    <option value="">-</option>
                                    <option value="1">Manufacturing</option>
                                    <option value="1">Shipping</option>
                                    <option value="1">Administration</option>
                                    <option value="1">Human Resources</option>
                                </select>
                            </div>
                            <div class="field half">
                                <input type="radio" id="demo-priority-low" name="demo-priority" checked>
                                <label for="demo-priority-low">Low</label>
                            </div>
                            <div class="field half">
                                <input type="radio" id="demo-priority-high" name="demo-priority">
                                <label for="demo-priority-high">High</label>
                            </div>
                            <div class="field half">
                                <input type="checkbox" id="demo-copy" name="demo-copy">
                                <label for="demo-copy">Email me a copy</label>
                            </div>
                            <div class="field half">
                                <input type="checkbox" id="demo-human" name="demo-human" checked>
                                <label for="demo-human">Not a robot</label>
                            </div>
                            <div class="field">
                                <label for="demo-message">Message</label>
                                <textarea name="demo-message" id="demo-message" placeholder="Enter your message"
                                    rows="6"></textarea>
                            </div>
                        </div>
                        <ul class="actions">
                            <li><input type="submit" value="Send Message" class="primary" /></li>
                            <li><input type="reset" value="Reset" /></li>
                        </ul>
                    </form>
                </section>

            </article>

        </div>

    </div>

    <!-- Scripts -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/browser.min.js"></script>
    <script src="assets/js/breakpoints.min.js"></script>
    <script src="assets/js/util.js"></script>
    <script src="assets/js/main.js"></script>

</body>

</html>