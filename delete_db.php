<?php
    /* Attempt MySQL server connection. Assuming you are running MySQL
    server with default setting (user 'root' with no password) */
    require_once "config.php";
                    
    ob_start();
    // Check connection
    if($mysqli === false){
        die("ERROR: Could not connect. " . $mysqli->connect_error);
    }

    // Attempt update query execution
    $sql = "DELETE  FROM alumnos";

    if($mysqli->query($sql) === true){
        echo "Records were updated successfully.";
    } else{
        echo "ERROR: Could not able to execute $sql. " . $mysqli->error;
    }

    // Close connection
    $mysqli->close();
    $host  = $_SERVER['HTTP_HOST'];
    $uri   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
    $extra = 'dashboard.php';
    header("Location: https://$host$uri/$extra", true, 307);
    ob_end_flush();

?>