<?php

session_start();
require_once "config.php";
ob_start();
if(!isset($_SESSION["username"])){ 
    $host  = $_SERVER['HTTP_HOST'];
    $uri   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
    $extra = 'login.php';
    header("Location: https://$host$uri/$extra", true, 307);
    ob_end_flush();
}


// Define variables and initialize with empty values
$username = $temp_username= $md5_password =$password = $confirm_password = "";
$username_err = $password_err = $confirm_password_err = ""; 

// Processing form data when form is submitted
if($_SERVER["REQUEST_METHOD"] == "POST")
{
    if(empty(trim($_POST["username"]))) {
        $username_err = "Please enter a username.";
    }
    else {
        $temp_username = trim($_POST["username"]);
        $sql = "SELECT * FROM users WHERE username = '$temp_username'";
        $result = mysqli_query($mysqli,$sql);
        $user = mysqli_fetch_assoc($result);
        if($user['username'] === $username) {
            $username_err = "\nThis username is already taken.";
        }else{
            $username = trim($_POST["username"]);
        }        
    }
        // Validate password
    if(empty(trim($_POST["password"]))){
        $password_err = "Favor de introducir una contraseña";     
    }
    
    if(strlen(trim($_POST["password"])) < 6){
        $password_err = "Password must have atleast 6 characters.";
    }
    else{
        $password = trim($_POST["password"]);
    }
        
    // Validate confirm password
    if(empty(trim($_POST["confirm_password"]))){
        $confirm_password_err = "Favor de Confirmar Contraseña.";     
    }
    else{
        $confirm_password = trim($_POST["confirm_password"]);
        if(empty($password_err) && ($password != $confirm_password)){
            $confirm_password_err = "Las contraseñas no coinciden.";
        }
    }
        
    // Check input errors before updating the database
    if(empty($username_err) && empty($password_err) && empty($confirm_password_err)){
        // Prepare an update statement
        $username = trim($_POST["username"]);
        $md5_password = md5($confirm_password);

        $sql = "INSERT INTO users (username, password) VALUES ('$username', '$md5_password')";
        
        $result = mysqli_query($mysqli,$sql); 
       
        if($result === TRUE){
            mysqli_close($mysqli);
            $host  = $_SERVER['HTTP_HOST'];
            $uri   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
            $extra = 'dashboard.php';
            header("Location: https://$host$uri/$extra", true, 307);
            ob_end_flush();
        }
        else{
            echo "Oops! Something went wrong. Please try again later.";
        }
    
    }
}
ob_end_flush();
?>

<!DOCTYPE HTML>

<html>

<head>
    <title>CPro- Registrar nuevo usuario</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
    <link rel="stylesheet" href="assets/css/main.css" />
    <noscript>
        <link rel="stylesheet" href="assets/css/noscript.css" />
    </noscript>

    <link rel="icon" type="image/png" sizes="16x16" href="assets/img/cpro-negro_Mesa-de-trabajo-1.webp">
    <link rel="icon" type="image/png" sizes="32x32" href="/assets/img/cpro-negro-02.png">
    <link rel="icon" type="image/png" sizes="181x180" href="/assets/img/cpro-negro-03.png">
    <link rel="icon" type="image/png" sizes="193x192" href="/assets/img/cpro-negro-04.png">

</head>

<body class="is-preload">

    <!-- Wrapper -->
    <div id="wrapper">

        <!-- Header -->
        <header id="header">
            <div class="logo">
                <img class="center" src="assets/img/Cpro-LOGO2-1-01.png" href="" alt="Cpro.mx"
                    style="display: center; width:60%;">
            </div>

            <div class="content">
                <div class="inner">
                    <h1>Crear Nuevo Usuario</h1>
                    <form action="<?= htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">

                        <div class="form-group" <?= (!empty($username_err)) ? 'has-error' : ''; ?>">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="fa fa-user form__text-inner"></i>
                                    </span>
                                </div>
                                <input type="text" class="form-control" name="username" placeholder="Usuario"
                                    value="<?= $username; ?>">
                            </div>
                            <span class="help-block"><?= $username_err; ?></span>
                        </div>

                        <div class="form-group" <?= (!empty($password)) ? 'has-error' : ''; ?>">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="fa fa-lock"></i>
                                    </span>
                                </div>
                                <input type="password" class="form-control" name="password" placeholder="Contraseña"
                                    value="<?= $password; ?>">
                            </div>
                            <span class="help-block"><?= $password; ?></span>
                        </div>
                        <div class="form-group" <?= (!empty($confirm_password_err)) ? 'has-error' : ''; ?>">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="fa fa-key"></i>
                                    </span>
                                </div>
                                <input type="password" class="form-control" name="confirm_password"
                                    placeholder="Confirm Password" value="<?= $confirm_password; ?>">
                            </div>
                            <span class="help-block"><?= $confirm_password_err; ?></span>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group mb-3">
                                    <button type="submit" style="margin-top:1rem;" class="btn btn-primary login-btn btn-block">Crear
                                        Usuario</button>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group mb-3">
                                    <a class="btn btn-primary login-btn btn-block"  style="margin-top:1rem;" href="dashboard.php">Cancel</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </header>

    </div>

    <!-- Scripts -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/browser.min.js"></script>
    <script src="assets/js/breakpoints.min.js"></script>
    <script src="assets/js/util.js"></script>
    <script src="assets/js/main.js"></script>

</body>

</html>