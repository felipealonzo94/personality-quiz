$(document).ready(function () {
  var current_fs, next_fs, previous_fs; //fieldsets
  var opacity;
  var user_submitted = false;
  var showingAlert = false;
  var partB_submitted = false;
  var partC_submitted = false;
  var partD_submitted = false;
  var personalities = {
    1: {
      name: "Realista",
      description:
        "La personalidad realista hace referencia a aquel patrón de conducta y pensamiento que tiende a ver el mundo como un todo objetivo y concreto. Se toman el mundo como les viene. Suelen ser realistas, dinámicos, materiales y aunque no son asociales el contacto con los demás no es para ellos lo más prioritario. También suelen ser pacientes y constantes. Este tipo de personalidades tienden a sentirse más a gusto desempeñando trabajos directos, con fuertes componentes prácticos y que exijan cierta motricidad y uso sistematizado de elementos.Suelen destacar en el uso de instrumentos mecánicos y con necesidad de precisión manual. Campos como la agricultura y la ganadería, la arquitectura o la ingeniería serían propicios para este tipo de personalidad.",
      imgSrc: "assets/img/PQ_Realista.png",
    },
    2: {
      name: "Investigador",
      description:
        "Este tipo de personalidad tiende más a la observación y al análisis del mundo, a menudo de una manera abstracta y intentando realizar asociaciones y encontrar relaciones entre los fenómenos que en él ocurren. Se trata de personalidades curiosas, analíticas, con tendencia a la introspección y al uso de la razón por encima de la emoción. No son especialmente sociables y suelen tener un enfoque del mundo más bien teórico, no interesándoles tanto la práctica. Esta personalidad se corresponde con tareas principalmente basadas en la investigación. Física, química, economía o biología son algunos de los ámbitos en que suelen observarse más este tipo de personalidades.",
      imgSrc: "assets/img/PQ_Investigador.png",
    },
    3: {
      name: "Social",
      description:
        "El aspecto más destacable de las personas con este tipo de personalidad es la necesidad o deseo de ayudar a otros a través del trato con ellos, y su elevada necesidad de interacción humana. Suele tratarse de personas muy empáticas y idealistas, altamente comunicativas y tener cierta facilidad o gusto para las relaciones y la cooperación.    El tipo de tareas en las que suele encontrarse este tipo de personalidad son todas aquellas que supongan un trato directo con otras personas y en que dicha interacción exista como objetivo la idea de dar apoyo al otro. Psicólogos, médicos, enfermeros, profesores o trabajadores sociales suelen tener características de este tipo de personalidad.Tareas más mecánicas no suelen ser de su agrado.",
      imgSrc: "assets/img/PQ_Social.png",
    },
    4: {
      name: "Convencional",
      description:
        "Estamos ante un tipo de personalidad que se caracteriza por el gusto por el orden sin necesidad de introducir grandes cambios en él. Tampoco precisan de un gran contacto social a nivel laboral. Suelen ser personas altamente organizadas, ordenadas, disciplinadas y formales. No es rara cierta tendencia al conformismo, dado que se identifican con la organización ya establecida. Suelen ser ágiles y lógicos. Dentro de este tipo de personalidades encontramos a personas con vocación por aspectos como la contabilidad, el trabajo en oficina, el secretariado, bibliotecarias/os… en general con tendencia a buscar el orden.",
      imgSrc: "assets/img/PQ_Convencional.png",
    },
    5: {
      name: "Emprendedor",
      description:
        "La capacidad de persuasión y la habilidad comunicativa son aspectos típicos de la personalidad emprendedora. Cierto nivel de dominancia y búsqueda de logro y poder son usuales en este tipo de personas, así como valor y capacidad de riesgo. Generalmente son personas con habilidades sociales y altamente extravertidos, con capacidad de liderazgo y un elevado nivel de energía. Profesiones en que prevalecen este tipo de personas son el mundo de la banca y de los negocios. Comerciales y empresarios suelen también tener rasgos de este tipo de personalidad.",
      imgSrc: "assets/img/PQ_Emprendedor.png",
    },
    6: {
      name: "Artístico",
      description:
        " creatividad y el uso de materiales en búsqueda de la expresión son algunos de los principales elementos que caracterizan la personalidad artística. No es raro que se trate de personas impulsivas, idealistas y altamente emotivas e intuitivas. La estética y poder proyectar hacia el mundo sus sensaciones es importante para ellos, y suelen ser personas independientes. Si bien también intentan ver el mundo desde la abstracción, suelen focalizarse más en la emoción y tiende a disgustarles lo meramente intelectual, poseyendo la necesidad de elaborar y crear. Pintores, escultores o músicos son algunos de los profesionales que tienden a este tipo de personalidad. También bailarines y actores, escritores y periodistas",
      imgSrc: "assets/img/PQ_Artistico.png",
    },
  };

  function isEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
  }

  $("#userForm .validate-me").each(function () {
    $(this)
      .children("input")
      .on("input", function () {
        if (user_submitted) {
          if ($(this).attr("id") === "email") {
            if (isEmail($(this).val().trim())) {
              $(this).removeClass("is-invalid");
              $(this).addClass("is-valid");
            } else {
              $(this).removeClass("is-valid");
              $(this).addClass("is-invalid");
            }
          } else {
            if ($(this).val().trim()) {
              $(this).removeClass("is-invalid");
              $(this).addClass("is-valid");
            } else {
              $(this).addClass("is-invalid");
              $(this).removeClass("is-valid");
            }
          }
        }
      });
  });

  $("#bForm .form-group").each(function () {
    $(this)
      .children("select")
      .change(function () {
        if (partB_submitted) {
          if ($(this).val()) {
            $(this).removeClass("is-invalid");
          }
        }
      });
  });

  $("#cForm .form-group").each(function () {
    $(this)
      .find("input[type=radio]")
      .each(function () {
        $(this).change(function () {
          if (partC_submitted) {
            if ($(this).val()) {
              $(this).parent().parent().removeClass("was-validated");
            }
          }
        });
      });
  });

  $("#dForm .form-group").each(function () {
    $(this)
      .find("input[type=radio]")
      .each(function () {
        $(this).change(function () {
          if (partD_submitted) {
            if ($(this).val()) {
              $(this).parent().parent().removeClass("was-validated");
            }
          }
        });
      });
  });

  function showNext() {
    if (!next_fs.hasClass("quiz-fs")) {
      //Add Class Active
      $("#progressbar li")
        .eq($("fieldset").not(".quiz-fs").index(next_fs))
        .addClass("active");
    }

    //show the next fieldset
    next_fs.show();
    //hide the current fieldset with style
    current_fs.animate(
      {
        opacity: 0,
      },
      {
        step: function (now) {
          // for making fielset appear animation
          opacity = 1 - now;

          current_fs.css({
            display: "none",
            position: "relative",
          });
          next_fs.css({
            opacity: opacity,
          });
        },
        duration: 600,
      }
    );
  }

  function processResult() {
    var a_checkeds = $("#aForm input:checked");
    var a_checkeds_1 = a_checkeds.filter(function () {
      return $(this).val() === "1";
    }).length;
    var a_checkeds_2 = a_checkeds.filter(function () {
      return $(this).val() === "2";
    }).length;
    var a_checkeds_3 = a_checkeds.filter(function () {
      return $(this).val() === "3";
    }).length;
    var a_checkeds_4 = a_checkeds.filter(function () {
      return $(this).val() === "4";
    }).length;
    var a_checkeds_5 = a_checkeds.filter(function () {
      return $(this).val() === "5";
    }).length;
    var a_checkeds_6 = a_checkeds.filter(function () {
      return $(this).val() === "6";
    }).length;

    var b_selects = $("#bForm select");
    var b_selects_1 = b_selects.filter(function () {
      return $(this).val() === "1";
    }).length;
    var b_selects_2 = b_selects.filter(function () {
      return $(this).val() === "2";
    }).length;
    var b_selects_3 = b_selects.filter(function () {
      return $(this).val() === "3";
    }).length;
    var b_selects_4 = b_selects.filter(function () {
      return $(this).val() === "4";
    }).length;
    var b_selects_5 = b_selects.filter(function () {
      return $(this).val() === "5";
    }).length;
    var b_selects_6 = b_selects.filter(function () {
      return $(this).val() === "6";
    }).length;

    var c_checkeds = $("#cForm input[type=radio]:checked");
    var c_checkeds_1 = c_checkeds.filter(function () {
      return $(this).val() === "1";
    }).length;
    var c_checkeds_2 = c_checkeds.filter(function () {
      return $(this).val() === "2";
    }).length;
    var c_checkeds_3 = c_checkeds.filter(function () {
      return $(this).val() === "3";
    }).length;
    var c_checkeds_4 = c_checkeds.filter(function () {
      return $(this).val() === "4";
    }).length;
    var c_checkeds_5 = c_checkeds.filter(function () {
      return $(this).val() === "5";
    }).length;
    var c_checkeds_6 = c_checkeds.filter(function () {
      return $(this).val() === "6";
    }).length;

    var d_checkeds = $("#dForm input[type=radio]:checked");
    var d_checkeds_1 = d_checkeds.filter(function () {
      return $(this).val() === "1";
    }).length;
    var d_checkeds_2 = d_checkeds.filter(function () {
      return $(this).val() === "2";
    }).length;
    var d_checkeds_3 = d_checkeds.filter(function () {
      return $(this).val() === "3";
    }).length;
    var d_checkeds_4 = d_checkeds.filter(function () {
      return $(this).val() === "4";
    }).length;
    var d_checkeds_5 = d_checkeds.filter(function () {
      return $(this).val() === "5";
    }).length;
    var d_checkeds_6 = d_checkeds.filter(function () {
      return $(this).val() === "6";
    }).length;

    var total_1 = a_checkeds_1 + b_selects_1 + c_checkeds_1 + d_checkeds_1;
    var total_2 = a_checkeds_2 + b_selects_2 + c_checkeds_2 + d_checkeds_2;
    var total_3 = a_checkeds_3 + b_selects_3 + c_checkeds_3 + d_checkeds_3;
    var total_4 = a_checkeds_4 + b_selects_4 + c_checkeds_4 + d_checkeds_4;
    var total_5 = a_checkeds_5 + b_selects_5 + c_checkeds_5 + d_checkeds_5;
    var total_6 = a_checkeds_6 + b_selects_6 + c_checkeds_6 + d_checkeds_6;

    var result = [
      ["1", total_1],
      ["2", total_2],
      ["3", total_3],
      ["4", total_4],
      ["5", total_5],
      ["6", total_6],
    ];

    result.sort(function (a, b) {
      return b[1] - a[1];
    });

    $("#firstPersonality").attr("src", personalities[result[0][0]].imgSrc);
    $("#firstResult").text(personalities[result[0][0]].name);
    $("#firstDescription").text(personalities[result[0][0]].description);
    $("#secondResult").text(personalities[result[1][0]].name);
    $("#thirdResult").text(personalities[result[2][0]].name);
  }

  $("#firstNext").click(function () {
    user_submitted = true;

    $("#userForm .validate-me input").each(function () {
      if ($(this).attr("id") === "email") {
        if (isEmail($(this).val().trim())) {
          $(this).addClass("is-valid");
        } else {
          $(this).addClass("is-invalid");
        }
      } else {
        if ($(this).val().trim()) {
          $(this).addClass("is-valid");
        } else {
          $(this).addClass("is-invalid");
        }
      }
    });

    if (
      !$("#name").val().trim() ||
      !isEmail($("#email").val().trim()) ||
      ($("#isEnroll").is(":checked") && !$("#invoice").val().trim())
    ) {
      return;
    }

    current_fs = $(this).parent().parent();
    next_fs = $(this).parent().parent().next();

    showNext();
  });

  $("#aNext").click(function () {
    if ($("#aForm input:checked").length === 0) {
      $("#alertChecked").removeClass("d-none");
      if (!showingAlert) {
        showingAlert = true;
        setTimeout(() => {
          $("#alertChecked").addClass("d-none");
          showingAlert = false;
        }, 3 * 1000);
      }
      return;
    }

    current_fs = $(this).parent().parent();
    next_fs = $(this).parent().parent().next();

    showNext();
  });

  $("#bNext").click(function () {
    var valid_B_part = true;
    partB_submitted = true;

    $("#bForm .form-group select").each(function () {
      if (!$(this).val()) {
        $(this).addClass("is-invalid");
        valid_B_part = false;
      }
    });

    if (!valid_B_part) {
      return;
    }

    current_fs = $(this).parent().parent();
    next_fs = $(this).parent().parent().next();

    showNext();
  });

  $("#cNext").click(function () {
    var valid_C_part = true;
    partC_submitted = true;

    $("#cForm .form-group").each(function () {
      var childs = $(this).find("input[type=radio]:checked");
      if (childs.length === 0) {
        $(this).addClass("was-validated");
        valid_C_part = false;
      }
    });

    if (!valid_C_part) {
      return;
    }

    current_fs = $(this).parent().parent();
    next_fs = $(this).parent().parent().next();

    showNext();
  });

  $("#dNext").click(function () {
    var valid_D_part = true;
    partD_submitted = true;

    $("#dForm .form-group").each(function () {
      var childs = $(this).find("input[type=radio]:checked");
      if (childs.length === 0) {
        $(this).addClass("was-validated");
        valid_D_part = false;
      }
    });

    if (!valid_D_part) {
      return;
    }

    current_fs = $(this).parent().parent();
    next_fs = $(this).parent().parent().next();

    processResult();
    showNext();
  });

  $(".next").click(function () {
    current_fs = $(this).parent().parent();
    next_fs = $(this).parent().parent().next();

    showNext();
  });

  $(".previous").click(function () {
    current_fs = $(this).parent().parent();
    previous_fs = $(this).parent().parent().prev();

    var index = $(current_fs).data("index");

    //Remove class active
    if (index) {
      $("#progressbar li").eq(index).removeClass("active");
    }

    //show the previous fieldset
    previous_fs.show();

    //hide the current fieldset with style
    current_fs.animate(
      {
        opacity: 0,
      },
      {
        step: function (now) {
          // for making fielset appear animation
          opacity = 1 - now;

          current_fs.css({
            display: "none",
            position: "relative",
          });
          previous_fs.css({
            opacity: opacity,
          });
        },
        duration: 600,
      }
    );
  });

  $(".radio-group .radio").click(function () {
    $(this).parent().find(".radio").removeClass("selected");
    $(this).addClass("selected");
  });

  $(".submit").click(function () {
    return false;
  });

  $("#isEnroll").change(function () {
    if ($(this).is(":checked")) {
      $("#invoiceContainer").removeClass("d-none");
    } else {
      $("#invoiceContainer").addClass("d-none");
    }
  });
});
