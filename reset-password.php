<?php

session_start();
require_once "config.php";

ob_start();
if(!isset($_SESSION["username"])){ 
    $host  = $_SERVER['HTTP_HOST'];
    $uri   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
    $extra = 'dashboard.php';
    header("Location: https://$host$uri/$extra", true, 307);
    ob_end_flush();
}

// Define variables and initialize with empty values
$new_password = $confirm_password = "";
$new_password_err = $confirm_password_err = "";
 
// Processing form data when form is submitted
if($_SERVER["REQUEST_METHOD"] == "POST")
{
    // Validate new password
    if(empty(trim($_POST["new_password"])))
    {
        $new_password_err = "Favor de introducir nueva contraseña.";     
    }
    elseif(strlen(trim($_POST["new_password"])) < 6)
    {
        $new_password_err = "La contraseña debe contener al menos 6 caracteres.";
    }
    else
    {
        $new_password = trim($_POST["new_password"]);
    }
    // Validate confirm password
    if(empty(trim($_POST["confirm_password"])))
    {
        $confirm_password_err = "Favor de Confirmar Contraseña.";
    }
    else
    {
        $confirm_password = trim($_POST["confirm_password"]);
        if(empty($new_password_err) && ($new_password != $confirm_password))
        {
            $confirm_password_err = "Las contraseñas no coinciden";
        }
    }
    // Check input errors before updating the database
    if(empty($new_password_err) && empty($confirm_password_err))
    {
        // Prepare an update statement

        $param_password = md5($new_password);
        $param_id = $_SESSION["username"];

        $sql = "UPDATE users SET password = '$param_password' WHERE username = '$param_id'";
        
        $result = mysqli_query($mysqli,$sql);
        
        if($result === TRUE){
            session_destroy();
            $host  = $_SERVER['HTTP_HOST'];
            $uri   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
            $extra = 'login.php';
            header("Location: https://$host$uri/$extra", true, 307);
            ob_end_flush();
        }
        else{
            echo "Oops! Something went wrong. Please try again later.";
        }
    mysqli_close($mysqli);
    }
}
ob_end_flush();
?>


<!DOCTYPE HTML>

<html>

<head>
    <title>CPro- Cambiar Contraseña</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
    <link rel="stylesheet" href="assets/css/main.css" />
    <noscript>
        <link rel="stylesheet" href="assets/css/noscript.css" />
    </noscript>

    <link rel="icon" type="image/png" sizes="16x16" href="assets/img/cpro-negro_Mesa-de-trabajo-1.webp">
    <link rel="icon" type="image/png" sizes="32x32" href="/assets/img/cpro-negro-02.png">
    <link rel="icon" type="image/png" sizes="181x180" href="/assets/img/cpro-negro-03.png">
    <link rel="icon" type="image/png" sizes="193x192" href="/assets/img/cpro-negro-04.png">

</head>

<body class="is-preload">

    <!-- Wrapper -->
    <div id="wrapper">

        <!-- Header -->
        <header id="header">
            <div class="logo">
                <img class="center" src="assets/img/Cpro-LOGO2-1-01.png" href="" alt="Cpro.mx"
                    style="display: center; width:60%;">
            </div>
            <div class="content">
                <div class="inner">
                    <h1>Cambiar Contraseña</h1>
                    <form action="<?= htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
                        <div class="form-group" <?= (!empty($new_password_err)) ? 'has-error' : ''; ?>">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="fa fa-lock"></i>
                                    </span>
                                </div>
                                <input type="password" class="form-control" name="new_password"
                                    placeholder="New Password" value="<?= $new_password; ?>">
                            </div>
                            <span class="help-block"><?= $new_password_err; ?></span>
                        </div>
                        <div class="form-group" <?= (!empty($confirm_password_err)) ? 'has-error' : ''; ?>">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="fa fa-key"></i>
                                    </span>
                                </div>
                                <input type="password" class="form-control" name="confirm_password"
                                    placeholder="Confirm Password" value="<?= $confirm_password; ?>">
                            </div>
                            <span class="help-block"><?= $confirm_password_err; ?></span>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group mb-3">
                                    <button type="submit" style="margin-top:1rem;" class="btn btn-primary login-btn btn-block">Reset
                                        Password</button>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group mb-3">
                                    <a class="btn btn-primary login-btn btn-block" style="margin-top:1rem;" href="dashboard.php">Cancel</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </header>

    </div>

    <!-- Scripts -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/browser.min.js"></script>
    <script src="assets/js/breakpoints.min.js"></script>
    <script src="assets/js/util.js"></script>
    <script src="assets/js/main.js"></script>

</body>

</html>