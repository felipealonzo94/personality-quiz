<?php
/*******EDIT LINES 3-8*******/
$DB_Server = "localhost"; //MySQL Server    
$DB_Username = "cpromx_personalityquiz"; //MySQL Username     
$DB_Password = "Cpro2021!";             //MySQL Password     
$DB_DBName = "cpromx_cpro_db";         //MySQL Database Name  
$DB_TBLName = "alumnos"; //MySQL Table Name   
/*******YOU DO NOT NEED TO EDIT ANYTHING BELOW THIS LINE*******/    
//create MySQL connection   

$conn = new mysqli($DB_Server, $DB_Username, $DB_Password, $DB_DBName);

if($conn === false){
    die("ERROR: Could not connect. " . $conn->connect_error);
}


$sql = "SELECT s.*, @rownum := @rownum + 1 AS num FROM alumnos s, (SELECT @rownum := 0) r";

$result = $conn->query($sql);

if(!$result){
    echo "ERROR: Could not able to execute $sql. " . $conn->error;
}


$salida = "";

$salida .= "<table>";
$salida .= "<thead> <th>ID</th> <th>Nombre Completo</th><th>Correo Electronico</th><th>Esta inscrito?</th><th>Folio</th><th>Fecha de Registro</th><th>Personalidad</th></thead>";
while($row = $result->fetch_assoc()) {
    $salida .= "<tr><td>" . $row["num"]. "</td><td>" . $row["name"] . "</td><td>" . $row["email"] . "</td><td>" . $row["is_join"] . "</td><td>" . $row["folio"]. "</td><td>" . $row["timestamp"]. "</td><td>". $row["personality"] . "</td></tr>";
}

$conn->close();

$date_now = date("dmYi"); 
$file_name_complete = "cpro_registro_quiz_{$date_now}.xls";
header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=$file_name_complete");
header("Pragma: no-cache");
header("Expires: 0");
echo $salida;

?>