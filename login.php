<?php
    ob_start();

    session_start();
    if(isset($_SESSION["username"])) { 
        $host  = $_SERVER['HTTP_HOST'];
        $uri   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
        $extra = 'dashboard.php';
        header("Location: https://$host$uri/$extra", true, 307);
        ob_end_flush();

    }

    // Include config file 
    require_once "config.php";

    $username = $password = "";
    $username_err = $password_err = "";


    if($_SERVER["REQUEST_METHOD"] == "POST"){
        $username = trim($_POST["username"]);

        if(empty(trim($_POST["username"]))) {
            $username_err = "Favor  de introducir usuario.";
        }
        else{
            $username = trim($_POST["username"]);
        }
        
        if(empty(trim($_POST["password"]))){
            $password_err = "Favor de introducir contraseña.";
        }
        else{
            $password = trim($_POST["password"]);
        }

        if(empty($username_err) && empty($password_err)){
            $sql = "SELECT username,password FROM users WHERE username='$username'";
            $result = mysqli_query($mysqli,$sql);
            $user = mysqli_fetch_assoc($result); 
            if($user){
                if($user['password'] === md5($password)){
                    session_start();
                    $_SESSION['username'] = $username;
                    $host  = $_SERVER['HTTP_HOST'];
                    $uri   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
                    $extra = 'dashboard.php';
                    header("Location: https://$host$uri/$extra", true, 307);
                    ob_end_flush();
                }
                else{
                    $password_err = "Contraseña incorrecta.";
                }
            }
            else{
                $username_err = "Usuario no existe.";
            }
        }

    }
    ob_end_flush();
?>


<!DOCTYPE HTML>

<html>

<head>
    <title>CPro - Login</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
    <link rel="stylesheet" href="assets/css/main.css" />
    <noscript>
        <link rel="stylesheet" href="assets/css/noscript.css" />
    </noscript>

    <link rel="icon" type="image/png" sizes="16x16" href="assets/img/cpro-negro_Mesa-de-trabajo-1.webp">
    <link rel="icon" type="image/png" sizes="32x32" href="/assets/img/cpro-negro-02.png">
    <link rel="icon" type="image/png" sizes="181x180" href="/assets/img/cpro-negro-03.png">
    <link rel="icon" type="image/png" sizes="193x192" href="/assets/img/cpro-negro-04.png">

</head>

<body class="is-preload">

    <!-- Wrapper -->
    <div id="wrapper">

        <!-- Header -->
        <header id="header">
            <div class="logo">
                <img class="center" src="assets/img/Cpro-LOGO2-1-01.png" href="" alt="Cpro.mx"
                    style="display: center; width:60%;">
            </div>

            <div class="content">
                <div class="inner">
                    <h1>Login</h1>
                    <p>Introduce tus datos de usuario</p>
                    <form action="<?= htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">

                        <div class="form-group" <?= (!empty($username_err)) ? 'has-error' : ''; ?>">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="fa fa-user form__text-inner"></i>
                                    </span>
                                </div>
                                <input type="text" class="form-control" name="username" placeholder="Usuario"
                                    value="<?= $username; ?>">
                            </div>
                            <span class="help-block"><?= $username_err; ?></span>
                        </div>

                        <div class="form-group" <?= (!empty($password)) ? 'has-error' : ''; ?>">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="fa fa-lock"></i>
                                    </span>
                                </div>
                                <input type="password" class="form-control" name="password" placeholder="Contraseña"
                                    value="<?= $password; ?>">
                            </div>
                            <span class="help-block"><?= $password; ?></span>
                        </div>

                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <button type="submit" style="margin-top:1rem;" class="btn btn-primary login-btn btn-block">Login</button>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <a class="btn btn-primary login-btn btn-block"  style="margin-top:1rem;" href="https://cpro.mx">Ir a cpro.mx</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </header>

    </div>

    <!-- Scripts -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/browser.min.js"></script>
    <script src="assets/js/breakpoints.min.js"></script>
    <script src="assets/js/util.js"></script>
    <script src="assets/js/main.js"></script>

</body>

</html>