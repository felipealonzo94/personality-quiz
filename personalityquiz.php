<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Quiz</title>
    <!-- jquery for maximum compatibility -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link rel="stylesheet" href="assets/css/personalityquizz.css">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Cpro- Cursos Propedéuticos en Mérida</title>

    <meta name="theme-color" content="#6B3FC2">
    <meta name="description" content="Te ayudamos a entrar a la Universidad y pasar el Exani II con los mejores Cursos Propedeutico de nuestra academia">
    <meta name="robots" content="index, follow, max-snippet:-1, max-image-preview:large, max-video-preview:-1">
    <link rel="canonical" href="https://cpro.mx/">
    <meta property="og:locale" content="es_Mx">
    <meta property="og:type" content="website">
    <meta property="og:title" content="Cpro - Entra a la Universidad de manera garantizada y pasa tu Exani 2 con nuestros Cursos Propedeuticos">
    <meta property="og:description" content="Te ayudamos a entrar a la Universidad y pasar el Exani II con los mejores Cursos Propedeutico">
    <meta property="og:url" content="https://cpro.mx/">
    <meta property="og:site_name" content="Cpro">
    <meta property="article:publisher" content="https://www.facebook.com/CProEXANI">
    <meta property="article:modified_time" content="2021-02-15T09:58:27+00:00">
    <meta property="og:image" content="https://cpro.mx/assets/img/cpro-2.webp">
    <meta property="og:image:width" content="1066">
    <meta property="og:image:height" content="800">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Cpro - Cursos Propedeuticos para pasar tu examen Exani II ">
    <meta name="twitter:description" content="Te ayudamos a entrar a la Universidad y pasar el Exani II con los mejores Cursos Propedeutico">
    <meta name="twitter:image" content="https://cpro.mx/assets/img/cpro-2.webp">
    <meta name="twitter:creator" content="@Cpro-Cursos-Propedeuticos">
    <meta name="twitter:site" content="@Cpro.mx">
    <meta name="twitter:label1" content="Escrito por">
    <meta name="twitter:data1" content="Cpro Cursos Propedeuticos">
    <meta name="twitter:label2" content="Est. reading time">
    <meta name="twitter:data2" content="19 minutes">
    <meta name="fb:app_id" content=" 472993253730002">

    <link rel="preload" href="assets/fonts/fontawesome5-overrides.min.css" as="style">
    <link rel="preload" href="assets/css/font-awesome.min.css" as="style">
    <link rel="preload" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous" as="style">
    <link rel="icon" type="image/png" sizes="16x16" href="assets/img/cpro-negro_Mesa-de-trabajo-1.webp">
    <link rel="icon" type="image/png" sizes="32x32" href="/assets/img/cpro-negro-02.png">
    <link rel="icon" type="image/png" sizes="181x180" href="/assets/img/cpro-negro-03.png">
    <link rel="icon" type="image/png" sizes="193x192" href="/assets/img/cpro-negro-04.png">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <link rel="manifest" href="manifest.json">

    <link rel="stylesheet" href="assets/css/styles.css">
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">

    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-NMG2H0F5XM"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'G-NMG2H0F5XM');
    </script>

    <script type="text/javascript">
        var hold = document.getElementById("divid");
        var checkbox = document.createElement('input');
        checkbox.type = "checkbox";
        checkbox.name = "chkbox1";
        checkbox.id = "cbid";
        var label = document.createElement('label');
        var tn = document.createTextNode("Not A RoBot");
        if (label) {
            label.htmlFor = "cbid";
            label.appendChild(tn);
        }
        if (hold) {
            hold.appendChild(label);
            hold.appendChild(checkbox);
        }
    </script>

    <!-- Cloudflare Web Analytics -->
    <script defer src='https://static.cloudflareinsights.com/beacon.min.js' data-cf-beacon='{"token": "9e2b2a3118434a25b41734ae2e63bdaf", "spa": true}'></script>
    <!-- End Cloudflare Web Analytics -->
</head>

<body>
    <div>
        <div class="container page-section" id="1">
            <div class="row" style="background: #e4ae0e;">
                <div class="col-7 col-sm-6 col-md-4 col-lg-3 col-xl-2 offset-2 offset-sm-3 offset-md-4 offset-lg-4 offset-xl-5">
                    <img loading="lazy" alt="Cursos Propedeuticos Utm" title=Cpro-logo src="assets/img/Cpro-a_Mesa_de_trabajo_1.png" width="50%" height="70%" style="margin: 9px;text-align: center;">
                </div>
            </div>
        </div>
    </div>

    <!--Quizz-->
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-11 my-3">
                <div class="card">
                    <div class="card-body">
                        <h2 class="card-title">Test de Orientacion Vocacional</h2>
                        <form id="msform"  action="">
                            <!-- progressbar -->
                            <ul id="progressbar">
                                <li class="active" id="description"><strong>Descripción</strong></li>
                                <li id="partA"><strong>Parte A</strong></li>
                                <li id="partB"><strong>Parte B</strong></li>
                                <li id="partC"><strong>Parte C</strong></li>
                                <li id="partD"><strong>Parte D</strong></li>
                                <li id="user"><strong>Info</strong></li>
                                <li id="results"><strong>Resultados</strong></li>
                            </ul>
                            <!-- fieldsets -->
                            <fieldset data-index="1">
                                <h4>Descripción del Quiz</h4>
                                <p>Reconocer el tipo de personalidad que predomina en nosotros es vital para elegir una carrera profesional que esté acorde con nuestras aptitudes y habilidades.</p>
                                <p>Usted puede ser de los que gustan de la lógica o de los que prefieren explotar más su creatividad, para cada una de las personalidades, existe una especialidad que les ayudará a explotar mejor su talento. En un estudio,
                                    realizado por el investigador John L. Holland, desarrolló seis tipos de personalidad que pudo conectar con diferentes especialidades profesionales, las cuales podras descubrir al finalizar este Quiz.</p>
                                <p><strong>Duración Aproximada ~10 min</strong></p>
                                <div class="text-center">
                                    <button type="button" name="previous" class="btn btn-outline-secondary previous">Anterior</button>
                                    <button type="button" name="next" class="btn btn-outline-primary next">Siguiente</button>
                                </div>
                               
                            </fieldset>
                            <fieldset data-index="2">
                                <h4>Instrucciones. Parte A.</h4>
                                <p>Selecciona todos los adjetivos que sientas que te describan. Señala tantos como desees. Trata de describirte tal como eres, no como te gustaría ser. No hay respuestas correctas o incorrectas, contesta con honestidad.</p>
                                <div class="text-center">
                                    <button type="button" name="previous" class="btn btn-outline-secondary previous">Anterior</button>
                                    <button type="button" name="next" class="btn btn-outline-primary next">Siguiente</button>
                                </div>
                            </fieldset>
                            <fieldset class="quiz-fs">
                                <h4>Autoconocimiento. Parte A.</h4>
                                <p><strong><em>Selecciona todos los adjetivos que sientas que te describan. Señala tantos como desees. Trata de describirte tal como eres, no como te gustaría ser.</em></strong></p>
                                <div id="alertChecked" class="alert alert-danger d-none" role="alert">
                                    Por favor, selecciona al menos una opción
                                </div>
                                <div class="form-row" id="aForm">
                                    <div class="form-group col-6 col-sm-3">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="qst1" value="6">
                                            <label class="custom-control-label" for="qst1">Huraño</label>
                                        </div>
                                    </div>
                                    <div class="form-group col-6 col-sm-3">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" id="qst2" class="custom-control-input" value="5">
                                            <label for="qst2" class="custom-control-label">Discutidor(a)</label>
                                        </div>
                                    </div>
                                    <div class="form-group col-6 col-sm-3">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" id="qst3" class="custom-control-input" value="1">
                                            <label for="qst3" class="custom-control-label">Arrogante</label>
                                        </div>
                                    </div>
                                    <div class="form-group col-6 col-sm-3">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" id="qst4" class="custom-control-input" value="3">
                                            <label for="qst4" class="custom-control-label">Capaz</label>
                                        </div>
                                    </div>
                                    <div class="form-group col-6 col-sm-3">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" id="qst5" class="custom-control-input" value="4">
                                            <label for="qst5" class="custom-control-label">Común y corriente</label>
                                        </div>
                                    </div>
                                    <div class="form-group col-6 col-sm-3">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" id="qst6" class="custom-control-input" value="4">
                                            <label for="qst6" class="custom-control-label">Conformista</label>
                                        </div>
                                    </div>
                                    <div class="form-group col-6 col-sm-3">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" id="qst7" class="custom-control-input" value="4">
                                            <label for="qst7" class="custom-control-label">Concienzudo(a)</label>
                                        </div>
                                    </div>
                                    <div class="form-group col-6 col-sm-3">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" id="qst8" class="custom-control-input" value="2">
                                            <label for="qst8" class="custom-control-label">Curioso</label>
                                        </div>
                                    </div>
                                    <div class="form-group col-6 col-sm-3">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" id="qst9" class="custom-control-input" value="4">
                                            <label for="qst9" class="custom-control-label">Dependiente</label>
                                        </div>
                                    </div>
                                    <div class="form-group col-6 col-sm-3">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" id="qst10" class="custom-control-input" value="4">
                                            <label for="qst10" class="custom-control-label">Eficiente</label>
                                        </div>
                                    </div>
                                    <div class="form-group col-6 col-sm-3">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" id="qst11" class="custom-control-input" value="1">
                                            <label for="qst11" class="custom-control-label">Paciente</label>
                                        </div>
                                    </div>
                                    <div class="form-group col-6 col-sm-3">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" id="qst12" class="custom-control-input" value="5">
                                            <label for="qst12" class="custom-control-label">Dinámico</label>
                                        </div>
                                    </div>
                                    <div class="form-group col-6 col-sm-3">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" id="qst13" class="custom-control-input" value="6">
                                            <label for="qst13" class="custom-control-label">Femenino(a)</label>
                                        </div>
                                    </div>
                                    <div class="form-group col-6 col-sm-3">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" id="qst14" class="custom-control-input" value="3">
                                            <label for="qst14" class="custom-control-label">Amistoso(a)</label>
                                        </div>
                                    </div>
                                    <div class="form-group col-6 col-sm-3">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" id="qst15" class="custom-control-input" value="3">
                                            <label for="qst15" class="custom-control-label">Generoso(a)</label>
                                        </div>
                                    </div>
                                    <div class="form-group col-6 col-sm-3">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" id="qst16" class="custom-control-input" value="3">
                                            <label for="qst16" class="custom-control-label">Dispuesto a ayudar</label>
                                        </div>
                                    </div>
                                    <div class="form-group col-6 col-sm-3">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" id="qst17" class="custom-control-input" value="3">
                                            <label for="qst17" class="custom-control-label">Inflexible</label>
                                        </div>
                                    </div>
                                    <div class="form-group col-6 col-sm-3">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" id="qst18" class="custom-control-input" value="1">
                                            <label for="qst18" class="custom-control-label">Insensible</label>
                                        </div>
                                    </div>
                                    <div class="form-group col-6 col-sm-3">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" id="qst19" class="custom-control-input" value="2">
                                            <label for="qst19" class="custom-control-label">Introvertido(a)</label>
                                        </div>
                                    </div>
                                    <div class="form-group col-6 col-sm-3">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" id="qst20" class="custom-control-input" value="6">
                                            <label for="qst20" class="custom-control-label">Intuitivo</label>
                                        </div>
                                    </div>
                                    <div class="form-group col-6 col-sm-3">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" id="qst21" class="custom-control-input" value="1">
                                            <label for="qst21" class="custom-control-label">Irritable</label>
                                        </div>
                                    </div>
                                    <div class="form-group col-6 col-sm-3">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" id="qst22" class="custom-control-input" value="3">
                                            <label for="qst22" class="custom-control-label">Amable</label>
                                        </div>
                                    </div>
                                    <div class="form-group col-6 col-sm-3">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" id="qst23" class="custom-control-input" value="5">
                                            <label for="qst23" class="custom-control-label">De buenos modales</label>
                                        </div>
                                    </div>
                                    <div class="form-group col-6 col-sm-3">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" id="qst24" class="custom-control-input" value="1">
                                            <label for="qst24" class="custom-control-label">Varonil</label>
                                        </div>
                                    </div>
                                    <div class="form-group col-6 col-sm-3">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" id="qst25" class="custom-control-input" value="6">
                                            <label for="qst25" class="custom-control-label">Inconforme</label>
                                        </div>
                                    </div>
                                    <div class="form-group col-6 col-sm-3">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" id="qst26" class="custom-control-input" value="4">
                                            <label for="qst26" class="custom-control-label">Poco realista</label>
                                        </div>
                                    </div>
                                    <div class="form-group col-6 col-sm-3">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" id="qst27" class="custom-control-input" value="1">
                                            <label for="qst27" class="custom-control-label">Poco culto(a)</label>
                                        </div>
                                    </div>
                                    <div class="form-group col-6 col-sm-3">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" id="qst28" class="custom-control-input" value="4">
                                            <label for="qst28" class="custom-control-label">Poco idealista</label>
                                        </div>
                                    </div>
                                    <div class="form-group col-6 col-sm-3">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" id="qst29" class="custom-control-input" value="2">
                                            <label for="qst29" class="custom-control-label">Impopular</label>
                                        </div>
                                    </div>
                                    <div class="form-group col-6 col-sm-3">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" id="qst30" class="custom-control-input" value="6">
                                            <label for="qst30" class="custom-control-label">Original</label>
                                        </div>
                                    </div>
                                    <div class="form-group col-6 col-sm-3">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" id="qst31" class="custom-control-input" value="2">
                                            <label for="qst31" class="custom-control-label">Pesimista</label>
                                        </div>
                                    </div>
                                    <div class="form-group col-6 col-sm-3">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" id="qst32" class="custom-control-input" value="5">
                                            <label for="qst32" class="custom-control-label">Hedonista</label>
                                        </div>
                                    </div>
                                    <div class="form-group col-6 col-sm-3">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" id="qst33" class="custom-control-input" value="2">
                                            <label for="qst33" class="custom-control-label">Práctico</label>
                                        </div>
                                    </div>
                                    <div class="form-group col-6 col-sm-3">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" id="qst34" class="custom-control-input" value="6">
                                            <label for="qst34" class="custom-control-label">Rebelde</label>
                                        </div>
                                    </div>
                                    <div class="form-group col-6 col-sm-3">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" id="qst35" class="custom-control-input" value="1">
                                            <label for="qst35" class="custom-control-label">Reservado</label>
                                        </div>
                                    </div>
                                    <div class="form-group col-6 col-sm-3">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" id="qst36" class="custom-control-input" value="2">
                                            <label for="qst36" class="custom-control-label">Culto</label>
                                        </div>
                                    </div>
                                    <div class="form-group col-6 col-sm-3">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" id="qst37" class="custom-control-input" value="2">
                                            <label for="qst37" class="custom-control-label">Lento de movimientos</label>
                                        </div>
                                    </div>
                                    <div class="form-group col-6 col-sm-3">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" id="qst38" class="custom-control-input" value="5">
                                            <label for="qst38" class="custom-control-label">Sociable</label>
                                        </div>
                                    </div>
                                    <div class="form-group col-6 col-sm-3">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" id="qst39" class="custom-control-input" value="5">
                                            <label for="qst39" class="custom-control-label">Estable</label>
                                        </div>
                                    </div>
                                    <div class="form-group col-6 col-sm-3">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" id="qst40" class="custom-control-input" value="5">
                                            <label for="qst40" class="custom-control-label">Esforzado</label>
                                        </div>
                                    </div>
                                    <div class="form-group col-6 col-sm-3">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" id="qst41" class="custom-control-input" value="5">
                                            <label for="qst41" class="custom-control-label">Fuerte</label>
                                        </div>
                                    </div>
                                    <div class="form-group col-6 col-sm-3">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" id="qst42" class="custom-control-input" value="4">
                                            <label for="qst42" class="custom-control-label">Suspicaz</label>
                                        </div>
                                    </div>
                                    <div class="form-group col-6 col-sm-3">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" id="qst43" class="custom-control-input" value="2">
                                            <label for="qst43" class="custom-control-label">Cumplido</label>
                                        </div>
                                    </div>
                                    <div class="form-group col-6 col-sm-3">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" id="qst44" class="custom-control-input" value="1">
                                            <label for="qst44" class="custom-control-label">Modesto</label>
                                        </div>
                                    </div>
                                    <div class="form-group col-6 col-sm-3">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" id="qst45" class="custom-control-input" value="6">
                                            <label for="qst45" class="custom-control-label">Poco convencional</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-center">
                                    <button type="button" name="previous" class="btn btn-outline-secondary previous">Anterior</button>
                                    <button type="button" name="next" id="aNext" class="btn btn-outline-primary">Siguiente</button>
                                </div>
                            </fieldset>
                            <fieldset data-index="3">
                                <h4>Instrucciones. Parte B.</h4>
                                <p>Selecciona la opción que más se ajuste a tí para cada concepto. Piensa en cómo eres con respecto a los jóvenes de tu edad, recuerda que no hay respuestas correctas o incorrectas, contesta con honestidad.</p>
                                <div class="text-center">
                                    <button type="button" name="previous" class="btn btn-outline-secondary previous">Anterior</button>
                                    <button type="button" name="next" class="btn btn-outline-primary next">Siguiente</button>
                                </div>
                            </fieldset>
                            <fieldset class="quiz-fs">
                                <h4>Parte B.</h4>
                                <p><strong><em>Selecciona la opción que más se ajuste a tí para cada concepto. Piensa en cómo eres con respecto a los jóvenes de tu edad.</em></strong></p>
                                <div class="form-row" id="bForm">
                                    <div class="form-group col-sm-6 col-md-4">
                                        <label for="slct1"><b>1.</b> Distraído(a)</label>
                                        <select class="custom-select" id="slct1" required>
                                            <option selected disabled value="">-- Selecciona una opción --</option>
                                            <option value="7">Más que los demás</option>
                                            <option value="1">Igual que los demás</option>
                                            <option value="1">Menos que los demás</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-sm-6 col-md-4"><label for="slct2"><b>2.</b>
                                            Capacidad artística</label>
                                        <select id="slct2" class="custom-select">
                                            <option selected disabled value="">-- Selecciona una opción --</option>
                                            <option value="6">Más que los demás</option>
                                            <option value="7">Igual que los demás</option>
                                            <option value="8">Menos que los demás</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-sm-6 col-md-4"><label for="slct3"><b>3.</b>
                                            Capacidad administrativa</label>
                                        <select id="slct3" class="custom-select">
                                            <option selected disabled value="">-- Selecciona una opción --</option>
                                            <option value="4">Más que los demás</option>
                                            <option value="7">Igual que los demás</option>
                                            <option value="8">Menos que los demás</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-sm-6 col-md-4"><label for="slct4"><b>4.</b>
                                            Conservador(a)</label>
                                        <select id="slct4" class="custom-select">
                                            <option selected disabled value="">-- Selecciona una opción --</option>
                                            <option value="4">Más que los demás</option>
                                            <option value="7">Igual que los demás</option>
                                            <option value="8">Menos que los demás</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-sm-6 col-md-4"><label for="slct5"><b>5.</b>
                                            Cooperación</label>
                                        <select id="slct5" class="custom-select">
                                            <option selected disabled value="">-- Selecciona una opción --</option>
                                            <option value="3">Más que los demás</option>
                                            <option value="7">Igual que los demás</option>
                                            <option value="8">Menos que los demás</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-sm-6 col-md-4"><label for="slct6"><b>6.</b>
                                            Expresividad</label>
                                        <select id="slct6" class="custom-select">
                                            <option selected disabled value="">-- Selecciona una opción --</option>
                                            <option value="6">Más que los demás</option>
                                            <option value="7">Igual que los demás</option>
                                            <option value="8">Menos que los demás</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-sm-6 col-md-4"><label for="slct7"><b>7.</b>
                                            Liderazgo</label>
                                        <select id="slct7" class="custom-select">
                                            <option selected disabled value="">-- Selecciona una opción --</option>
                                            <option value="5">Más que los demás</option>
                                            <option value="7">Igual que los demás</option>
                                            <option value="8">Menos que los demás</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-sm-6 col-md-4"><label for="slct8"><b>8.</b>
                                            Gusto en ayudar a los demás</label>
                                        <select id="slct8" class="custom-select">
                                            <option selected disabled value="">-- Selecciona una opción --</option>
                                            <option value="3">Más que los demás</option>
                                            <option value="7">Igual que los demás</option>
                                            <option value="8">Menos que los demás</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-sm-6 col-md-4"><label for="slct9"><b>9.</b>
                                            Capacidad matemática</label>
                                        <select id="slct9" class="custom-select">
                                            <option selected disabled value="">-- Selecciona una opción --</option>
                                            <option value="2">Más que los demás</option>
                                            <option value="7">Igual que los demás</option>
                                            <option value="8">Menos que los demás</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-sm-6 col-md-4"><label for="slct10"><b>10.</b>
                                            Capacidad mecánica</label>
                                        <select id="slct10" class="custom-select">
                                            <option selected disabled value="">-- Selecciona una opción --</option>
                                            <option value="1">Más que los demás</option>
                                            <option value="7">Igual que los demás</option>
                                            <option value="8">Menos que los demás</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-sm-6 col-md-4"><label for="slct11"><b>11.</b>
                                            Originalidad</label>
                                        <select id="slct11" class="custom-select">
                                            <option selected disabled value="">-- Selecciona una opción --</option>
                                            <option value="6">Más que los demás</option>
                                            <option value="7">Igual que los demás</option>
                                            <option value="8">Menos que los demás</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-sm-6 col-md-4"><label for="slct12"><b>12.</b>
                                            Popularidad con las personas que te atraen</label>
                                        <select id="slct12" class="custom-select">
                                            <option selected disabled value="">-- Selecciona una opción --</option>
                                            <option value="5">Más que los demás</option>
                                            <option value="7">Igual que los demás</option>
                                            <option value="8">Menos que los demás</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-sm-6 col-md-4"><label for="slct13"><b>13.</b>
                                            Capacidad para investigar</label>
                                        <select id="slct13" class="custom-select">
                                            <option selected disabled value="">-- Selecciona una opción --</option>
                                            <option value="2">Más que los demás</option>
                                            <option value="7">Igual que los demás</option>
                                            <option value="8">Menos que los demás</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-sm-6 col-md-4"><label for="slct14"><b>14.</b>
                                            Capacidad científica</label>
                                        <select id="slct14" class="custom-select">
                                            <option selected disabled value="">-- Selecciona una opción --</option>
                                            <option value="2">Más que los demás</option>
                                            <option value="7">Igual que los demás</option>
                                            <option value="8">Menos que los demás</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-sm-6 col-md-4"><label for="slct15"><b>15.</b>
                                            Seguridad en ti mismo(a)</label>
                                        <select id="slct15" class="custom-select">
                                            <option selected disabled value="">-- Selecciona una opción --</option>
                                            <option value="5">Más que los demás</option>
                                            <option value="7">Igual que los demás</option>
                                            <option value="8">Menos que los demás</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-sm-6 col-md-4"><label for="slct16"><b>16.</b>
                                            Comprensión de ti mismo(a)</label>
                                        <select id="slct16" class="custom-select">
                                            <option selected disabled value="">-- Selecciona una opción --</option>
                                            <option value="7">Más que los demás</option>
                                            <option value="1">Igual que los demás</option>
                                            <option value="1">Menos que los demás</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-sm-6 col-md-4"><label for="slct17"><b>17.</b>
                                            Comprensión de los demás</label>
                                        <select id="slct17" class="custom-select">
                                            <option selected disabled value="">-- Selecciona una opción --</option>
                                            <option value="3">Más que los demás</option>
                                            <option value="7">Igual que los demás</option>
                                            <option value="8">Menos que los demás</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-sm-6 col-md-4"><label for="slct18"><b>18.</b>
                                            Cuidadoso(a), limpio(a), minucioso(a)</label>
                                        <select id="slct18" class="custom-select">
                                            <option selected disabled value="">-- Selecciona una opción --</option>
                                            <option value="4">Más que los demás</option>
                                            <option value="7">Igual que los demás</option>
                                            <option value="8">Menos que los demás</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="text-center">
                                    <button type="button" name="previous" class="btn btn-outline-secondary previous">Anterior</button>
                                    <button type="button" name="next" id="bNext" class="btn btn-outline-primary">Siguiente</button>
                                </div>
                            </fieldset>
                            <fieldset data-index="4">
                                <h4>Instrucciones. Parte C.</h4>
                                <p>Selecciona la opción más adecuada para ti en cada inciso. Piensa ¿Qué tan importantes son las siguientes propuestas de metas, logros y aspiraciones? Recuerda que no hay respuestas correctas o incorrectas, contesta con honestidad.</p>
                                <div class="text-center">
                                    <button type="button" name="previous" class="btn btn-outline-secondary previous">Anterior</button>
                                    <button type="button" name="next" class="btn btn-outline-primary next">Siguiente</button>
                                </div>
                            </fieldset>
                            <fieldset class="quiz-fs">
                                <h4>Parte C.</h4>
                                <p><strong><em>Selecciona la opción más adecuada para ti en cada inciso. Piensa ¿Qué tan importantes son las siguientes propuestas de metas, logros y aspiraciones?</em></strong></p>
                                <div class="form-row my-4" id="cForm">
                                    <div class="form-group col-md-6">
                                        <label class="d-block"><b>1.</b> Estar feliz y satisfecho(a)</label>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="rad1_1" name="rad1" value="4" required class="custom-control-input">
                                            <label class="custom-control-label" for="rad1_1">Muy importante</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="rad1_2" name="rad1" value="7" required class="custom-control-input">
                                            <label class="custom-control-label" for="rad1_2">Más o menos
                                                importante</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="rad1_3" name="rad1" value="7" required class="custom-control-input">
                                            <label class="custom-control-label" for="rad1_3">Poco importante</label>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="d-block"><b>2.</b> Descubrir o elaborar un producto apto y de
                                            provecho</label>
                                        <div class="custom-control custom-radio custom-control-inline"><input type="radio" name="rad2" id="rad2_1" value="1" required class="custom-control-input"><label for="rad2_1" class="custom-control-label">Muy importante</label></div>
                                        <div class="custom-control custom-radio custom-control-inline"><input type="radio" name="rad2" id="rad2_2" value="7" required class="custom-control-input"><label for="rad2_2" class="custom-control-label">Más o menos importante</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline"><input type="radio" name="rad2" id="rad2_3" value="8" required class="custom-control-input"><label for="rad2_3" class="custom-control-label">Poco importante</label></div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="d-block"><b>3.</b> Ayudar a quiénes están en apuros</label>
                                        <div class="custom-control custom-radio custom-control-inline"><input type="radio" name="rad3" id="rad3_1" value="3" required class="custom-control-input"><label for="rad3_1" class="custom-control-label">Muy importante</label></div>
                                        <div class="custom-control custom-radio custom-control-inline"><input type="radio" name="rad3" id="rad3_2" value="7" required class="custom-control-input"><label for="rad3_2" class="custom-control-label">Más o menos importante</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline"><input type="radio" name="rad3" id="rad3_3" value="8" required class="custom-control-input"><label for="rad3_3" class="custom-control-label">Poco importante</label></div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="d-block"><b>4.</b> Llegar a ser una autoridad en algún
                                            tema</label>
                                        <div class="custom-control custom-radio custom-control-inline"><input type="radio" name="rad4" id="rad4_1" value="2" required class="custom-control-input"><label for="rad4_1" class="custom-control-label">Muy importante</label></div>
                                        <div class="custom-control custom-radio custom-control-inline"><input type="radio" name="rad4" id="rad4_2" value="7" required class="custom-control-input"><label for="rad4_2" class="custom-control-label">Más o menos importante</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline"><input type="radio" name="rad4" id="rad4_3" value="8" required class="custom-control-input"><label for="rad4_3" class="custom-control-label">Poco importante</label></div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="d-block"><b>5.</b> Llegar a ser un deportista destacado</label>
                                        <div class="custom-control custom-radio custom-control-inline"><input type="radio" name="rad5" id="rad5_1" value="1" required class="custom-control-input"><label for="rad5_1" class="custom-control-label">Muy importante</label></div>
                                        <div class="custom-control custom-radio custom-control-inline"><input type="radio" name="rad5" id="rad5_2" value="7" required class="custom-control-input"><label for="rad5_2" class="custom-control-label">Más o menos importante</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline"><input type="radio" name="rad5" id="rad5_3" value="8" required class="custom-control-input"><label for="rad5_3" class="custom-control-label">Poco importante</label></div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="d-block"><b>6.</b> Llegar a ser un líder de comunidad</label>
                                        <div class="custom-control custom-radio custom-control-inline"><input type="radio" name="rad6" id="rad6_1" value="5" required class="custom-control-input"><label for="rad6_1" class="custom-control-label">Muy importante</label></div>
                                        <div class="custom-control custom-radio custom-control-inline"><input type="radio" name="rad6" id="rad6_2" value="7" required class="custom-control-input"><label for="rad6_2" class="custom-control-label">Más o menos importante</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline"><input type="radio" name="rad6" id="rad6_3" value="8" required class="custom-control-input"><label for="rad6_3" class="custom-control-label">Poco importante</label></div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="d-block"><b>7.</b> Ser influyente en asuntos públicos</label>
                                        <div class="custom-control custom-radio custom-control-inline"><input type="radio" name="rad7" id="rad7_1" value="5" required class="custom-control-input"><label for="rad7_1" class="custom-control-label">Muy importante</label></div>
                                        <div class="custom-control custom-radio custom-control-inline"><input type="radio" name="rad7" id="rad7_2" value="7" required class="custom-control-input"><label for="rad7_2" class="custom-control-label">Más o menos importante</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline"><input type="radio" name="rad7" id="rad7_3" value="8" required class="custom-control-input"><label for="rad7_3" class="custom-control-label">Poco importante</label></div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="d-block"><b>8.</b> Observar una conducta religiosa formal</label>
                                        <div class="custom-control custom-radio custom-control-inline"><input type="radio" name="rad8" id="rad8_1" value="4" required class="custom-control-input"><label for="rad8_1" class="custom-control-label">Muy importante</label></div>
                                        <div class="custom-control custom-radio custom-control-inline"><input type="radio" name="rad8" id="rad8_2" value="7" required class="custom-control-input"><label for="rad8_2" class="custom-control-label">Más o menos importante</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline"><input type="radio" name="rad8" id="rad8_3" value="8" required class="custom-control-input"><label for="rad8_3" class="custom-control-label">Poco importante</label></div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="d-block"><b>9.</b> Contribuir a la ciencia en forma
                                            teórica</label>
                                        <div class="custom-control custom-radio custom-control-inline"><input type="radio" name="rad9" id="rad9_1" value="2" required class="custom-control-input"><label for="rad9_1" class="custom-control-label">Muy importante</label></div>
                                        <div class="custom-control custom-radio custom-control-inline"><input type="radio" name="rad9" id="rad9_2" value="7" required class="custom-control-input"><label for="rad9_2" class="custom-control-label">Más o menos importante</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline"><input type="radio" name="rad9" id="rad9_3" value="8" required class="custom-control-input"><label for="rad9_3" class="custom-control-label">Poco importante</label></div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="d-block"><b>10.</b> Contribuir a la ciencia en forma técnica
                                            (práctica)</label>
                                        <div class="custom-control custom-radio custom-control-inline"><input type="radio" name="rad10" id="rad10_1" value="2" required class="custom-control-input"><label for="rad10_1" class="custom-control-label">Muy importante</label></div>
                                        <div class="custom-control custom-radio custom-control-inline"><input type="radio" name="rad10" id="rad10_2" value="7" required class="custom-control-input"><label for="rad10_2" class="custom-control-label">Más o menos importante</label></div>
                                        <div class="custom-control custom-radio custom-control-inline"><input type="radio" name="rad10" id="rad10_3" value="8" required class="custom-control-input"><label for="rad10_3" class="custom-control-label">Poco importante</label></div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="d-block"><b>11.</b> Escribir bien (novelas, poemas)</label>
                                        <div class="custom-control custom-radio custom-control-inline"><input type="radio" name="rad11" id="rad11_1" value="6" required class="custom-control-input"><label for="rad11_1" class="custom-control-label">Muy importante</label></div>
                                        <div class="custom-control custom-radio custom-control-inline"><input type="radio" name="rad11" id="rad11_2" value="7" required class="custom-control-input"><label for="rad11_2" class="custom-control-label">Más o menos importante</label></div>
                                        <div class="custom-control custom-radio custom-control-inline"><input type="radio" name="rad11" id="rad11_3" value="8" required class="custom-control-input"><label for="rad11_3" class="custom-control-label">Poco importante</label></div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="d-block"><b>12.</b> Haber leído mucho</label>
                                        <div class="custom-control custom-radio custom-control-inline"><input type="radio" name="rad12" id="rad12_1" value="7" required class="custom-control-input"><label for="rad12_1" class="custom-control-label">Muy importante</label></div>
                                        <div class="custom-control custom-radio custom-control-inline"><input type="radio" name="rad12" id="rad12_2" value="1" required class="custom-control-input"><label for="rad12_2" class="custom-control-label">Más o menos importante</label></div>
                                        <div class="custom-control custom-radio custom-control-inline"><input type="radio" name="rad12" id="rad12_3" value="1" required class="custom-control-input"><label for="rad12_3" class="custom-control-label">Poco importante</label></div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="d-block"><b>13.</b> Trabajar mucho</label>
                                        <div class="custom-control custom-radio custom-control-inline"><input type="radio" name="rad13" id="rad13_1" value="4" required class="custom-control-input"><label for="rad13_1" class="custom-control-label">Muy importante</label></div>
                                        <div class="custom-control custom-radio custom-control-inline"><input type="radio" name="rad13" id="rad13_2" value="7" required class="custom-control-input"><label for="rad13_2" class="custom-control-label">Más o menos importante</label></div>
                                        <div class="custom-control custom-radio custom-control-inline"><input type="radio" name="rad13" id="rad13_3" value="8" required class="custom-control-input"><label for="rad13_3" class="custom-control-label">Poco importante</label></div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="d-block"><b>14.</b> Contribuir al bienestar humano.</label>
                                        <div class="custom-control custom-radio custom-control-inline"><input type="radio" name="rad14" id="rad14_1" value="3" required class="custom-control-input"><label for="rad14_1" class="custom-control-label">Muy importante</label></div>
                                        <div class="custom-control custom-radio custom-control-inline"><input type="radio" name="rad14" id="rad14_2" value="7" required class="custom-control-input"><label for="rad14_2" class="custom-control-label">Más o menos importante</label></div>
                                        <div class="custom-control custom-radio custom-control-inline"><input type="radio" name="rad14" id="rad14_3" value="8" required class="custom-control-input"><label for="rad14_3" class="custom-control-label">Poco importante</label></div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="d-block"><b>15.</b> Crear buenas obras artísticas (teatro,
                                            pintura)</label>
                                        <div class="custom-control custom-radio custom-control-inline"><input type="radio" name="rad15" id="rad15_1" value="6" required class="custom-control-input"><label for="rad15_1" class="custom-control-label">Muy importante</label></div>
                                        <div class="custom-control custom-radio custom-control-inline"><input type="radio" name="rad15" id="rad15_2" value="7" required class="custom-control-input"><label for="rad15_2" class="custom-control-label">Más o menos importante</label></div>
                                        <div class="custom-control custom-radio custom-control-inline"><input type="radio" name="rad15" id="rad15_3" value="8" required class="custom-control-input"><label for="rad15_3" class="custom-control-label">Poco importante</label></div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="d-block"><b>16.</b> Llegar a ser un buen músico</label>
                                        <div class="custom-control custom-radio custom-control-inline"><input type="radio" name="rad16" id="rad16_1" value="6" required class="custom-control-input"><label for="rad16_1" class="custom-control-label">Muy importante</label></div>
                                        <div class="custom-control custom-radio custom-control-inline"><input type="radio" name="rad16" id="rad16_2" value="7" required class="custom-control-input"><label for="rad16_2" class="custom-control-label">Más o menos importante</label></div>
                                        <div class="custom-control custom-radio custom-control-inline"><input type="radio" name="rad16" id="rad16_3" value="8" required class="custom-control-input"><label for="rad16_3" class="custom-control-label">Poco importante</label></div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="d-block"><b>17.</b> Llegar a ser un experto en finanzas y
                                            negocios</label>
                                        <div class="custom-control custom-radio custom-control-inline"><input type="radio" name="rad17" id="rad17_1" value="5" required class="custom-control-input"><label for="rad17_1" class="custom-control-label">Muy importante</label></div>
                                        <div class="custom-control custom-radio custom-control-inline"><input type="radio" name="rad17" id="rad17_2" value="7" required class="custom-control-input"><label for="rad17_2" class="custom-control-label">Más o menos importante</label></div>
                                        <div class="custom-control custom-radio custom-control-inline"><input type="radio" name="rad17" id="rad17_3" value="8" required class="custom-control-input"><label for="rad17_3" class="custom-control-label">Poco importante</label></div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="d-block"><b>18.</b> Hallar un propósito real en la vida</label>
                                        <div class="custom-control custom-radio custom-control-inline"><input type="radio" name="rad18" id="rad18_1" value="3" required class="custom-control-input"><label for="rad18_1" class="custom-control-label">Muy importante</label></div>
                                        <div class="custom-control custom-radio custom-control-inline"><input type="radio" name="rad18" id="rad18_2" value="7" required class="custom-control-input"><label for="rad18_2" class="custom-control-label">Más o menos importante</label></div>
                                        <div class="custom-control custom-radio custom-control-inline"><input type="radio" name="rad18" id="rad18_3" value="8" required class="custom-control-input"><label for="rad18_3" class="custom-control-label">Poco importante</label></div>
                                    </div>
                                </div>
                                <div class="text-center">
                                    <button type="button" name="previous" class="btn btn-outline-secondary previous">Anterior</button>
                                    <button type="button" name="next" id="cNext" class="btn btn-outline-primary">Siguiente</button>
                                </div>
                            </fieldset>
                            <fieldset data-index="5">
                                <h4>Instrucciones. Parte D.</h4>
                                <p>Para las siguientes preguntas escoja una sola alternativa, según lo que más se ajuste a tu experiencia. Recuerda que nadie te juzga, no hay respuestas correctas ni incorrectas. Responde con honestidad.</p>
                                <div class="text-center">
                                    <button type="button" name="previous" class="btn btn-outline-secondary previous">Anterior</button>
                                    <button type="button" name="next" class="btn btn-outline-primary next">Siguiente</button>
                                </div>
                            </fieldset>
                            <fieldset class="quiz-fs">
                                <h4>Parte D.</h4>
                                <p><strong><em>Para las siguientes preguntas escoja una sola alternativa, según lo que más se ajuste a tu experiencia.</em></strong></p>
                                <div class="form-row my-4" id="dForm">
                                    <div class="form-group col-md-6">
                                        <label><b>1.</b> Me gusta...</label>
                                        <div class="custom-control custom-radio">
                                            <input type="radio" id="rad_D1_1" name="rad_D1" value="2" required class="custom-control-input">
                                            <label class="custom-control-label" for="rad_D1_1">Leer y meditar sobre
                                                los problemas</label>
                                        </div>
                                        <div class="custom-control custom-radio">
                                            <input type="radio" id="rad_D1_2" name="rad_D1" value="4" required class="custom-control-input">
                                            <label class="custom-control-label" for="rad_D1_2">Anotar datos y hacer
                                                cómputos (cálculos)</label>
                                        </div>
                                        <div class="custom-control custom-radio">
                                            <input type="radio" id="rad_D1_3" name="rad_D1" value="5" required class="custom-control-input">
                                            <label class="custom-control-label" for="rad_D1_3">Tener una posición de
                                                poder o estatus</label>
                                        </div>
                                        <div class="custom-control custom-radio">
                                            <input type="radio" id="rad_D1_4" name="rad_D1" value="3" required class="custom-control-input">
                                            <label class="custom-control-label" for="rad_D1_4">Enseñar o ayudar a
                                                los demás</label>
                                        </div>
                                        <div class="custom-control custom-radio">
                                            <input type="radio" id="rad_D1_5" name="rad_D1" value="1" required class="custom-control-input">
                                            <label class="custom-control-label" for="rad_D1_5">Trabajar manualmente,
                                                usar equipos, herramientas</label>
                                        </div>
                                        <div class="custom-control custom-radio">
                                            <input type="radio" id="rad_D1_6" name="rad_D1" value="6" required class="custom-control-input">
                                            <label class="custom-control-label" for="rad_D1_6">Usar mi talento
                                                artístico</label>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label><b>2.</b> Mi mayor habilidad se manifiesta en...</label>
                                        <div class="custom-control custom-radio">
                                            <input type="radio" id="rad_D2_1" name="rad_D2" value="4" required class="custom-control-input">
                                            <label class="custom-control-label" for="rad_D2_1">Negocios</label>
                                        </div>
                                        <div class="custom-control custom-radio">
                                            <input type="radio" id="rad_D2_2" name="rad_D2" value="6" required class="custom-control-input">
                                            <label class="custom-control-label" for="rad_D2_2">Artes</label>
                                        </div>
                                        <div class="custom-control custom-radio">
                                            <input type="radio" id="rad_D2_3" name="rad_D2" value="2" required class="custom-control-input">
                                            <label class="custom-control-label" for="rad_D2_3">Ciencias</label>
                                        </div>
                                        <div class="custom-control custom-radio">
                                            <input type="radio" id="rad_D2_4" name="rad_D2" value="5" required class="custom-control-input">
                                            <label class="custom-control-label" for="rad_D2_4">Liderazgo</label>
                                        </div>
                                        <div class="custom-control custom-radio">
                                            <input type="radio" id="rad_D2_5" name="rad_D2" value="3" required class="custom-control-input">
                                            <label class="custom-control-label" for="rad_D2_5">Relaciones
                                                Humanas</label>
                                        </div>
                                        <div class="custom-control custom-radio">
                                            <input type="radio" id="rad_D2_6" name="rad_D2" value="1" required class="custom-control-input">
                                            <label class="custom-control-label" for="rad_D2_6">Mecánica</label>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label><b>3.</b> Soy muy incompetente en...</label>
                                        <div class="custom-control custom-radio">
                                            <input type="radio" id="rad_D3_1" name="rad_D3" value="3" required class="custom-control-input">
                                            <label class="custom-control-label" for="rad_D3_1">Mecánica</label>
                                        </div>
                                        <div class="custom-control custom-radio">
                                            <input type="radio" id="rad_D3_2" name="rad_D3" value="5" required class="custom-control-input">
                                            <label class="custom-control-label" for="rad_D3_2">Ciencia</label>
                                        </div>
                                        <div class="custom-control custom-radio">
                                            <input type="radio" id="rad_D3_3" name="rad_D3" value="1" required class="custom-control-input">
                                            <label class="custom-control-label" for="rad_D3_3">Relaciones
                                                Humanas</label>
                                        </div>
                                        <div class="custom-control custom-radio">
                                            <input type="radio" id="rad_D3_4" name="rad_D3" value="6" required class="custom-control-input">
                                            <label class="custom-control-label" for="rad_D3_4">Negocios</label>
                                        </div>
                                        <div class="custom-control custom-radio">
                                            <input type="radio" id="rad_D3_5" name="rad_D3" value="2" required class="custom-control-input">
                                            <label class="custom-control-label" for="rad_D3_5">Liderazgo</label>
                                        </div>
                                        <div class="custom-control custom-radio">
                                            <input type="radio" id="rad_D3_6" name="rad_D3" value="4" required class="custom-control-input">
                                            <label class="custom-control-label" for="rad_D3_6">Artes</label>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label><b>4.</b> Si tuviera que realizar alguna de estas actividades, la que
                                            menos me agradaría es...</label>
                                        <div class="custom-control custom-radio">
                                            <input type="radio" id="rad_D4_1" name="rad_D4" value="5" required class="custom-control-input">
                                            <label class="custom-control-label" for="rad_D4_1">Tener una posición de
                                                responsabilidad</label>
                                        </div>
                                        <div class="custom-control custom-radio">
                                            <input type="radio" id="rad_D4_2" name="rad_D4" value="1" required class="custom-control-input">
                                            <label class="custom-control-label" for="rad_D4_2">Llevar pacientes mentales
                                                a actividades recreativas</label>
                                        </div>
                                        <div class="custom-control custom-radio">
                                            <input type="radio" id="rad_D4_3" name="rad_D4" value="6" required class="custom-control-input">
                                            <label class="custom-control-label" for="rad_D4_3">Llevar registros exactos
                                                y complejos</label>
                                        </div>
                                        <div class="custom-control custom-radio">
                                            <input type="radio" id="rad_D4_4" name="rad_D4" value="4" required class="custom-control-input">
                                            <label class="custom-control-label" for="rad_D4_4">Escribir un poema</label>
                                        </div>
                                        <div class="custom-control custom-radio">
                                            <input type="radio" id="rad_D4_5" name="rad_D4" value="3" required class="custom-control-input">
                                            <label class="custom-control-label" for="rad_D4_5">Hacer algo que exija
                                                paciencia y precisión</label>
                                        </div>
                                        <div class="custom-control custom-radio">
                                            <input type="radio" id="rad_D4_6" name="rad_D4" value="2" required class="custom-control-input">
                                            <label class="custom-control-label" for="rad_D4_6">Participar en actividades
                                                sociales muy formales</label>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label><b>5.</b> Las materias que más me gustan son...</label>
                                        <div class="custom-control custom-radio">
                                            <input type="radio" id="rad_D5_1" name="rad_D5" value="6" required class="custom-control-input">
                                            <label class="custom-control-label" for="rad_D5_1">Arte</label>
                                        </div>
                                        <div class="custom-control custom-radio">
                                            <input type="radio" id="rad_D5_2" name="rad_D5" value="4" required class="custom-control-input">
                                            <label class="custom-control-label" for="rad_D5_2">Administración,
                                                contabilidad</label>
                                        </div>
                                        <div class="custom-control custom-radio">
                                            <input type="radio" id="rad_D5_3" name="rad_D5" value="2" required class="custom-control-input">
                                            <label class="custom-control-label" for="rad_D5_3">Química, Física</label>
                                        </div>
                                        <div class="custom-control custom-radio">
                                            <input type="radio" id="rad_D5_4" name="rad_D5" value="1" required class="custom-control-input">
                                            <label class="custom-control-label" for="rad_D5_4">Educación tecnológica,
                                                Mecánica</label>
                                        </div>
                                        <div class="custom-control custom-radio">
                                            <input type="radio" id="rad_D5_5" name="rad_D5" value="5" required class="custom-control-input">
                                            <label class="custom-control-label" for="rad_D5_5">Historia</label>
                                        </div>
                                        <div class="custom-control custom-radio">
                                            <input type="radio" id="rad_D5_6" name="rad_D5" value="3" required class="custom-control-input">
                                            <label class="custom-control-label" for="rad_D5_6">Ciencias sociales,
                                                Filosofía</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-center">
                                    <button type="button" name="previous" class="btn btn-outline-secondary previous">Anterior</button>
                                    <button type="button" name="next" id="dNext" class="btn btn-outline-primary">Finalizar</button>
                                </div>
                            </fieldset>
                            <fieldset>
                                <h4>Información</h4>
                                <div class="form-row" id="userForm">
                                <p><u> Completa los siguientes campos con tu información para calcular tus resultados. Muchas Gracias por realizar este quiz.</u></p>
                                    <div class="form-group col-sm-6 validate-me">
                                        <label for="name">Nombre</label>
                                        <input type="text" class="form-control" name="name" id="name" required>
                                        <div class="invalid-feedback">Este campo es requerido</div>
                                    </div>
                                    <div class="form-group col-sm-6 validate-me">
                                        <label for="email">Email</label>
                                        <input type="email" name="email" class="form-control" id="email" required>
                                        <div class="invalid-feedback">Email inválido</div>
                                    </div>
                                    <div class="form-group col-12">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="isEnroll">
                                            <label class="custom-control-label" for="isEnroll">¿Estás incrito?</label>
                                        </div>
                                    </div>
                                    <div class="form-group col-sm-6 d-none validate-me" id="invoiceContainer">
                                        <label for="invoice">Folio</label>
                                        <input type="text" name="invoice" class="form-control" id="invoice" required>
                                        <div class="invalid-feedback">Folio inválido</div>
                                    </div>
                                </div>
                                <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="termsandconditions" onchange="document.getElementById('firstNext').disabled = !this.checked;">
                                            <label class="custom-control-label" for="termsandconditions">Aceptar el <a href="privacy.html">aviso de privacidad</a> y los <a href="terminos-condiciones.html">terminos y condiciones</a> de este sitio web</label>
                                </div>
                                
                                <div class="text-center">
                                    <button type="button"  name="next" id="firstNext" class="btn btn-outline-primary" disabled>Siguiente</button>
                                </div>
                            </fieldset>
                            <fieldset>
                                <div class="text-center">
                                    <h4>¡Has completado el cuestionario satisfactoriamente!</h4>
                                    <img src="https://img.icons8.com/color/96/000000/ok--v2.png">
                                    <p class="mt-3">Haz click en el botón de abajo para ver tus resultados</p>
                                    <button  type="button" id="dbUpdate" name="next" class="btn btn-outline-success next">Ver Resultados</button>
                                </div>
                            </fieldset>
                            <fieldset class="quiz-fs">
                                <div class="row p-3">
                                    <div class="col-sm-4 text-center">
                                        <img id="firstPersonality" class="img-fluid mb-3 mb-sm-0" src="assets/img/PQ_Artistico.png">
                                    </div>
                                    <div class="col">
                                        <h4>1. <span id="firstResult" name="firstResult" class="firstResult"></span></h4>
                                        <p class="text-justify" id="firstDescription"></p>
                                        <h5>2. <span id="secondResult"></span></h5>
                                        <h5>3. <span id="thirdResult"></span></h5>
                                    </div>
                                </div>
                            </fieldset>
                            <script>
                                    $(document).ready(function() { 
                                        $("#dbUpdate").click(function() {
                                            var name = $("#name").val();
                                            var email = $("#email").val();
                                            var invoice;
                                            var is_join;
                                            var personality = $('#firstResult').html();

                                            ckb = $("#isEnroll").is(':checked');
                                            if (ckb == true) {
                                                invoice = $("#invoice").val();
                                                is_join = 1;
                                            } else {
                                                is_join = 0;
                                                invoice = 0;
                                            }
                                            $.ajax({
                                                url: 'guardar.php',
                                                method: 'POST',
                                                data: {
                                                    name: name,
                                                    email: email,
                                                    invoice: invoice,
                                                    is_join: is_join,
                                                    personality:personality
                                                }
                                            });
                                        });
                                    });
                                </script>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--Footer-->
    <div>
        <footer class="footer-section">
            <div class="container">
                <div class="footer-content pt-5 pb-5">
                    <div class="row">
                        <div class="col-xl-4 col-lg-4 mb-50">
                            <div class="footer-widget">
                                <div class="footer-logo">
                                    <a rel="noopener" href="index.html"><img src="assets/img/cpro-blanco-03.webp" class="img-fluid" alt="logo" width="180px" height="180px"></a>
                                </div>
                                <div class="footer-text">
                                    <h6>¿Estas listo para tu siguiente reto? Te preparamos para que llegues a tus metas y entres a la universidad que desees Contáctanos</h6>
                                </div>
                            </div>
                        </div>

                        <div class="col-xl-4 col-lg-4 col-md-6 mb-30">
                            <div class="footer-widget">
                                <div class="footer-widget-heading">
                                    <h6>
                                        <bold>Preguntas Frecuentes</bold>
                                    </h6>
                                </div>
                                <ul>
                                    <li><a rel="noopener" href="https://www.youtube.com/watch?v=cr_kA9Od-eQ">¿Qué
                                            garantías tengo con ustedes?</a></li>
                                    <li><a rel="noopener" href="#Nosotros">¿Quienes Somos?</a></li>
                                    <li><a rel="noopener" href="https://cpro.mx/blog/que-son-los-curso-propedeutico/">¿Qué
                                            son los Curso Propedéutico?</a></li>

                                </ul>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-6 mb-50">
                            <div class="footer-widget">
                                <div class="footer-social-icon">
                                    <span>Contáctanos</span>

                                    <a href="https://www.facebook.com/CProEXANI" target="_blank" rel="noopener" class="fa fa-facebook " alt=Cpro-facebook title=Cpro-facebook></a>
                                    <a href="https://www.youtube.com/channel/UCdH5wHVzmohqzy5FXbKMAwg" target="_blank" rel="noopener" class="fa fa-youtube " alt=Cpro-Youtube title=Cpro-Youtube></a>
                                    <a href="https://www.instagram.com/cursospropedeuticoscpro/" target="_blank" rel="noopener" class="fa fa-instagram " alt=Cpro-instagram title=Cpro-instagram></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="copyright-area">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-6 col-lg-6 text-center text-lg-left">
                            <div class="copyright-text">
                                <p>Copyright &copy; 2021, Derechos Reservados <a rel="noopener" href="https://cpro.mx/">CPRO Cursos Propedeuticos</a></p>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 d-none d-lg-block text-right">
                            <div class="footer-menu">
                                <ul>
                                    <li><a rel="noopener" href="privacy.html">Privacidad</a></li>
                                    <li><a rel="noopener" href="terminos-condiciones.html">Terminos y Condiciones</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/bs-init.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/personality_quiz.js"></script>
</body>

</html>