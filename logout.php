<?php
// Initialize the session
    session_start();

    ob_start();
    // Unset all of the session variables
    $_SESSION = array();
    
    // Destroy the session.
    session_destroy();
 
    // Redirect to login page
    $host  = $_SERVER['HTTP_HOST'];
    $uri   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
    $extra = 'login.php';
    header("Location: https://$host$uri/$extra", true, 307);
    ob_end_flush();
?>
